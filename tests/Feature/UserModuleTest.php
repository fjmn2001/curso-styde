<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserModuleTest extends TestCase
{
    /** @test */
    function it_show_the_users_list()
    {
        $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee('Lista de usuarios')
            ->assertSee('Francisco')
            ->assertSee('Edith');
    }

    /** @test */
    function it_show_a_default_message_if_the_users_list_is_empty()
    {
        $this->get('/usuarios?empty')
            ->assertStatus(200)
            ->assertSee('Lista de usuarios')
            ->assertDontSee('Francisco')
            ->assertDontSee('Edith')
            ->assertSee('No hay usuarios registrados.');
    }

    /** @test */
    function it_loads_the_user_dateils_page()
    {
        $this->get('/usuarios/7')
            ->assertStatus(200)
            ->assertSee('User details 7');
    }

    /** @test */
    function it_loads_the_users_new_page()
    {
        $this->get('/usuarios/nuevo')
            ->assertStatus(200)
            ->assertSee('User new page');
    }
}
