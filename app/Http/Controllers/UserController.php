<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->has('empty')){
            $users = [
                'Francisco',
                'Edith'
            ];
        } else {
            $users = [];
        }
        $title = 'Lista de usuarios';

        return view('users', compact('users', 'title'));
    }

    public function show($id)
    {
        return "User details {$id}";
    }

    public function create()
    {
        return "User new page";
    }
}
